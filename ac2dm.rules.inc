<?php
/**
 * @file
 * The rules hook and actions
 *
 * Define each action, messages to users, etc.
 */

/**
 * Implementation of hook_rules_action_info().
 */
function ac2dm_rules_action_info() {
  return array(
    'ac2dm_action_send_message' => array(
      'label' => t('Send AC2DM message'),
      'module' => 'AC2DM',
      'eval input' => array('tokens', 'keyValue'),
    ),
  );
}


function ac2dm_action_send_message($settings) {

    // Get form values   
    $tokens = strip_tags($settings['tokens']);
    $keyValues = $settings['keyValue'];
    watchdog("ac2dm", "AC2DM keyValues=" . var_export($keyValues, true));

    $ac2dm_message = array();
    foreach (explode(",", $keyValues) as $keyValue) {
      $temp = explode("=", $keyValue);
      $key = $temp[0];
      $value = $temp[1];
      if (isset($key) && isset($value)) {
        $ac2dm_message[$key] = $value;
      }
    }
    watchdog("ac2dm", "AC2DM array message=" . var_export($ac2dm_message, true));
    ac2dm_send_message(array_unique(explode(',', $tokens)), $ac2dm_message);
}
