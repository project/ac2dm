<?php

/**
 * @file
 * The forms for the rules
 *
 * Standard definition via the Form API
 */

/**
 * Action drupal message configuration form.
 */
function ac2dm_action_send_message_form($settings = array(), &$form) {

  $form['settings']['tokens'] = array(
    '#type' => 'textfield',
    '#title' => t('Tokens'),
    '#default_value' => $settings['tokens'],
    '#description' => t("Tokens, comma delimeter"),
  );
  $form['settings']['keyValue'] = array(
    '#type' => 'textfield',
    '#title' => t('Key-Value'),
    '#default_value' => $settings['keyValue'],
    '#description' => t("Comma delimetered key-value pairs, for example key1=value1,key2=value"),
  );
}
