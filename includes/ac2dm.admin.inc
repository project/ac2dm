<?php

/**
 * @file
 * Admin files for Push Notifications module.
 */

/**
 * Form callback for Push Notifications Settings.
 */
function ac2dm_admin_form($form_state) {
  $form = array();

  $form['configuration_ac2dm'] = array(
    '#type' => 'fieldset',
    '#title' => t('AC2DM Push Notifications'),
    '#description' => t('Requires a valid AC2DM Google Account. !signup to enable a Google Account for AC2DM.', array('!signup' => l(t('Signup here'), 'http://code.google.com/android/ac2dm/signup.html', array('attributes' => array('target' => '_blank'))))),
  );
  
  $form['configuration_ac2dm']['ac2dm_username'] = array(
    '#type' => 'textfield',
    '#title' => t('AC2DM Username'),
    '#description' => t('Enter the username for your AC2DM Google Account'),
    '#default_value' => variable_get('ac2dm_username', ''),
  );

  $form['configuration_ac2dm']['ac2dm_password'] = array(
    '#type' => 'textfield',
    '#title' => t('AC2DM Password'),
    '#description' => t('Enter the password for your AC2DM Google Account'),
    '#default_value' => variable_get('ac2dm_password', ''),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Configuration',
  );

  return $form;
}



/**
 * Submit callback for push notification configuration page.
 */
function ac2dm_admin_form_submit($form, &$form_state) {
  // Set AC2DM credentials.
  variable_set('ac2dm_username', $form_state['values']['ac2dm_username']);
  variable_set('ac2dm_password', $form_state['values']['ac2dm_password']);
}

/**
 * Submit handler for sending out a mass-push notification.
 */
function ac2dm_mass_push_form_submit($form, &$form_state) {
  $recipients = $form_state['values']['recipients'];
  $payload = $form_state['values']['payload'];

  // Send message to all Android recipients.
  if (!empty($recipients['android'])) {
    // Get all Android recipients.
    $tokens_android = ac2dm_get_tokens();
    if (!empty($tokens_android)) {
      $result = ac2dm_send_message($tokens_android, $payload);
      $dsm_type = ($result['success']) ? 'status' : 'error';
      drupal_set_message($result['message'], $dsm_type);
    }
    else {
      drupal_set_message(t('No Android recipients found.'));
    }
  }
}
