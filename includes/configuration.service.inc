<?php

/**
 * @file
 * Services callbacks.
 */

/**
 * Service callback to retrieve configuration values
 *
 *
 * @return
 *   Sender ID
 */
function _configuration_service_retrieve_values($variable) {
  if (empty($variable)) {
    return services_error(t('Configuration variable is missing.'), 400);
  }

  if ($variable === 'ac2dm_username') {
    $sender_id = variable_get('ac2dm_username');

    if (empty($sender_id)) {
      // Log the error
      watchdog('ac2dm', 'Device tried to retrieve your AC2DM Username, and it is yet to be defined by you.', NULL, WATCHDOG_ERROR);

      return services_error(t('AC2DM Username / Sender ID is not defined yet.'), 400);
    }
    else {
      return array('ac2dm_username' => $sender_id);
    }
  } else {
      return services_error(t('Configuration !name not supported.', array('!name' => $variable)), 400);
  }
}